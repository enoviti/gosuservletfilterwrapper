/**
 * MIT License
 * 
 * Copyright (c) 2021 Enoviti, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.enoviti.common.servlet;

import javax.servlet.*;
import java.io.IOException;

/**
 * This is a wrapper class used to load Gosu Servlet Filter at runtime. The Gosu
 * class name can be provided using filter initialization parameter. For
 * example:
 * 
 * {@code}
 * <filter> 
 * 		<filter-name>YourServletFilter</filter-name>
 * 		<filter-class>com.enoviti.common.servlet.GosuFilterLoader</filter-class>
 * 		<init-param> 
 * 			<param-name>GosuFilterClassName</param-name>
 * 			<param-value>yourcompany.servlet.GosuServletFilter</param-value> 
 * 		</init-param>
 * </filter>
 * {@code}
 * 
 * @author apurohit
 *
 */
public class GosuServletFilterLoader implements Filter {
	private org.slf4j.Logger _logger = org.slf4j.LoggerFactory.getLogger(GosuServletFilterLoader.class);
	private String _gosuClassName;
	private Filter _gosuFilter;
	private FilterConfig cfg;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.cfg = filterConfig;
		_logger.trace("GosuFilterLoader.init()...Enter");

		String gosuClassName = filterConfig.getInitParameter("GosuFilterClassName");
		if (gosuClassName != null && gosuClassName.length() > 0) {
			_gosuClassName = gosuClassName.trim();
		}

		_logger.trace("GosuFilterLoader.init()...Exit");
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		getGosuFilter().doFilter(servletRequest, servletResponse, filterChain);
	}

	@Override
	public void destroy() {
		if (_gosuFilter != null)
			_gosuFilter.destroy();
	}

	private synchronized Filter getGosuFilter() throws ServletException {
		if (this._gosuFilter == null && _gosuClassName != null) {
			_logger.debug("Loading Gosu filter class " + _gosuClassName);

			try {
				_gosuFilter = gw.lang.reflect.ReflectUtil.construct(_gosuClassName);
			} catch (Exception ex) {
				_logger.error("Error loading class! Please check the GosuFilterClassName in web.xml", ex);
				throw new ServletException("Error loading class! Please check the GosuFilterClassName in web.xml", ex);
			}
			_gosuFilter.init(cfg);
		}

		return _gosuFilter;
	}

}